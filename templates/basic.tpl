<center>
	<h1>{$nombre_cancion}</h1>
	<span style="color:gray;">Cantidad de veces vista: {$veces_buscada}</span>

	<br/><br/>
	
	{$letra_html}

	<br/><br/>

	<p>
		Otras letras del mismo artista:
		<a href="{$letra_1_link}">{$letra_1_caption}</a> | 
		<a href="{$letra_2_link}">{$letra_2_caption}</a> | 
		<a href="{$letra_3_link}">{$letra_3_caption}</a> 
	</p>
</center>
